export default interface TodoItem {
    title: string;
    description: string;
    done: boolean;
}
