import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/home/home.vue'),
    },
    {
      path: '/settings',
      name: 'settings',
      component: () => import('./views/settings/settings.vue'),
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/login/login.vue'),
    },
  ],
});
